package ru.sbt.mipt.oop;

public interface EventHandler {

    void handleSmartHomeEvent(SensorEvent event);
}