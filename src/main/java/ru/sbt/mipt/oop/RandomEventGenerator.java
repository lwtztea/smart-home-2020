package ru.sbt.mipt.oop;

public class RandomEventGenerator implements SensorEventGenerator {

    public SensorEvent getEvent() {

        if (Math.random() < 0.05) return null; // null means end of event stream
        SensorEventType sensorEventType = SensorEventType.values()[(int) (4 * Math.random())];
        String objectId = "" + ((int) (10 * Math.random()));
        return new SensorEvent(sensorEventType, objectId);
    }
}