package ru.sbt.mipt.oop;

public interface SensorEventGenerator {

    SensorEvent getEvent();
}
